#!/bin/sh

# Ce script va effectuer la migration de la base de donnée. 
# Il devrait être exécuté une seule fois après avoir lancé docker-compose up -d dans le dossier du projet.
docker run -v /root/docker/migration:/repo --network leflot --rm -e ASPNETCORE_ENVIRONMENT=Production mcr.microsoft.com/dotnet/sdk:5.0 sh -c \
  "dotnet tool install --global dotnet-ef && \
  export PATH="$PATH:/root/.dotnet/tools" && \
  dotnet ef database update -p /repo/src/LeFlot -s /repo/src/LeFlot.Api"
