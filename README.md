# LeFlot.Deploy

Ceci est le dépôt de déploiement pour l'API et le site web du projet Le Flot.

# Machine virtuelle

Le plus simple pour mettre en place le serveur web et l'API est de les installer sur une machine virtuelle dans le cloud qui supporte Docker, Docker-compose, et un runner de GitLab pour lier la machine au code de GitLab et ainsi automatiser le déploiement. Notre choix s'est tourné vers DigitalOcean qui offre toutes ces options et coûte environ 5USD/mois pour la machine la plus simple de leur catalogue.

Pour installer une machine virtuelle de manière générale
Docker et Docker-compose doivent être installés.
Un Runner de GitLab doit lier la machine à GitLab pour tirer les instructions pour générer la machine virtuelle.
Il faut lier le domaine leflot.art à la machine virtuelle.

Les mots de passe de l'API sont définies dans les variables sur GitLab dans la section LeFlot.Deploy Settings/CI/CD.
